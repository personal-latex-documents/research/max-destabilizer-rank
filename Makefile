# Requires GNU make, xargs, a latex distribution, sage
# and sagetex.sty visible in TEXINPUTS
#
# This Makefile is responsible for generating files required by latexmk, but
# should not be responsible for anything actually involving latex compilation

# Stuff that can be built before ever running latex:
pretex: rust_tex notebook_py_libs

# Location for all the python libraries generated from the jupyter notebooks:
GENERATED_PYLIBS_DIR=generated_py_libs

# PYTHON LIBS FROM NOTEBOOKS

NOTEBOOKBASENAMES = $(basename $(notdir $(wildcard notebooks/*.ipynb)))

define NOTEBOOK_TO_LIB_RULE
$(addprefix $(GENERATED_PYLIBS_DIR)/, $(1).py): $(addprefix notebooks/, $(1).ipynb)
	jupyter nbconvert --to script $(addprefix notebooks/, $(1).ipynb) --output-dir=$(GENERATED_PYLIBS_DIR)
	cd $(GENERATED_PYLIBS_DIR) &&\
	mv $(1).py $(1).sage &&\
	sed -e "/get_ipython/d" -i $(1).sage &&\
	sage --preparse $(1).sage &&\
	mv $(1).sage.py $(1).py
endef

$(GENERATED_PYLIBS_DIR):
	mkdir $@

$(foreach _base, $(NOTEBOOKBASENAMES), $(eval $(call NOTEBOOK_TO_LIB_RULE, $(_base))))

notebook_py_libs: $(addprefix $(GENERATED_PYLIBS_DIR)/, $(addsuffix .py, $(NOTEBOOKBASENAMES)))

# TEX FILES FROM RUST

define RUST_TO_TEX_RULE
$(1).rs.tex.git-untrack: $(1).rs
	chromacode -r -f -i $(1).rs -o $(1).rs.tex.git-untrack
endef

RUSTSNIPPETSBASE=tilt.rs/src/tilt_stability/considered_b_for_beta.git-untrack tilt.rs/src/tilt_stability/find_all.git-untrack tilt.rs/src/tilt_stability/left_pseudo_semistabilizers/find_all.git-untrack tilt.rs/src/tilt_stability/left_pseudo_semistabilizers/fixed_q_beta/find_all.git-untrack tilt.rs/src/tilt_stability/left_pseudo_semistabilizers/fixed_q_beta/possible_chern2.git-untrack
RUSTBASENAMES=$(sort $(basename $(wildcard tilt.rs/src/*.rs)) $(basename $(wildcard tilt.rs/src/**/*.rs)) ${RUSTSNIPPETSBASE})

$(foreach _base, $(RUSTBASENAMES), $(eval $(call RUST_TO_TEX_RULE, $(_base))))

rust_tex: $(addsuffix .rs.tex.git-untrack, $(RUSTBASENAMES))

# specific snippets of rust
#
tilt.rs/src/tilt_stability/considered_b_for_beta.git-untrack.rs: tilt.rs/src/tilt_stability/left_pseudo_semistabilizers.rs
	sed -n -e '25,37p' $^ > $@

tilt.rs/src/tilt_stability/find_all.git-untrack.rs: tilt.rs/src/tilt_stability/left_pseudo_semistabilizers.rs
	sed -n -e '81,101p' $^ > $@

tilt.rs/src/tilt_stability/left_pseudo_semistabilizers/find_all.git-untrack.rs: tilt.rs/src/tilt_stability/left_pseudo_semistabilizers/fixed_q_beta.rs
	sed -n -e '128,137p' $^ > $@

tilt.rs/src/tilt_stability/left_pseudo_semistabilizers/fixed_q_beta/find_all.git-untrack.rs: tilt.rs/src/tilt_stability/left_pseudo_semistabilizers/fixed_q_beta/fixed_r.rs
	sed -n -e '37,42p' $^ > $@

tilt.rs/src/tilt_stability/left_pseudo_semistabilizers/fixed_q_beta/possible_chern2.git-untrack.rs: tilt.rs/src/tilt_stability/left_pseudo_semistabilizers/fixed_q_beta/fixed_r.rs
	sed -n -e '44,69p' $^ > $@

# MISCELANEOUS

.PHONY: clean nosage noappendix
clean:
	rm -rf **/__pycache__
	latexmk -C
	git clean -xf || echo no git repo to use for cleaning

nosage:
	latexmk

noappendix: ${TEXFILES} ${SAGETEXARTIFACT}
	latexmk
