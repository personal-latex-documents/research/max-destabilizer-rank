\section{Characteristic Curves of Stability Conditions Associated to Chern
Characters}

% NOTE: SURFACE SPECIALIZATION
Considering the stability conditions with two parameters $\alpha, \beta$ on
Picard rank 1 surfaces.
We can draw 2 characteristic curves for any given Chern character $v$ with
$\Delta(v) \geq 0$ and positive rank.
These are given by the equations $\chern_i^{\alpha,\beta}(v)=0$ for $i=1,2$.

\begin{definition}[Characteristic Curves $V_v$ and $\Theta_v$]
Given a Chern character $v$, with positive rank and $\Delta(v) \geq 0$, we
define two characteristic curves on the $(\alpha, \beta)$-plane:

\begin{align*}
	V_v &\colon \:\: \chern_1^{\alpha, \beta}(v) = 0 \\
	\Theta_v &\colon \:\: \chern_2^{\alpha, \beta}(v) = 0
\end{align*}
\end{definition}

\subsection{Geometry of the Characteristic Curves}

These characteristic curves for a Chern character $v$ with $\Delta(v)\geq0$ are
not affected by flipping the sign of $v$ so it's only necessary to consider
non-negative rank.
As discussed in Subsection \ref{subsect:relevance-of-V_v}, making this choice
has Gieseker stable coherent sheaves appearing in the heart of the stability
condition $\firsttilt{\beta}$ as we move `left' (decreasing $\beta$).

\subsubsection{Positive Rank Case}
\label{subsect:positive-rank-case-charact-curves}

\begin{fact}[Geometry of Characteristic Curves in Positive Rank Case]
The following facts can be deduced from the formulae for $\chern_i^{\alpha, \beta}(v)$
as well as the restrictions on $v$, when $\chern_0(v)>0$:
\begin{itemize}
	\item $V_v$ is a vertical line at $\beta=\mu(v)$
	\item $\Theta_v$ is a hyperbola with assymptotes angled at $\pm 45^\circ$
		crossing where $V_v$ meets the $\beta$-axis: $(\mu(v),0)$
	\item $\Theta_v$ is oriented with left-right branches (as opposed to up-down).
		The left branch shall be labelled $\Theta_v^-$ and the right $\Theta_v^+$.
	\item The gap along the $\beta$-axis between either branch of $\Theta_v$
		and $V_v$ is $\sqrt{\Delta(v)}/\chern_0(v)$.
	\item When $\Delta(v)=0$, $\Theta_v$ degenerates into a pair of lines, but the
		labels $\Theta_v^\pm$ will still be used for convenience.
\end{itemize}
\end{fact}

These are illustrated in Fig \ref{fig:charact_curves_vis}
(dotted line for $i=1$, solid for $i=2$).

\begin{sagesilent}
from characteristic_curves import \
typical_characteristic_curves, \
degenerate_characteristic_curves
\end{sagesilent}


\begin{figure}
\centering
\begin{subfigure}{.49\textwidth}
	\centering
	\sageplot[width=\textwidth]{typical_characteristic_curves}
	\caption{$\Delta(v)>0$}
	\label{fig:charact_curves_vis_bgmvlPos}
\end{subfigure}%
\hfill
\begin{subfigure}{.49\textwidth}
	\centering
	\sageplot[width=\textwidth]{degenerate_characteristic_curves}
	\caption{
		$\Delta(v)=0$: hyperbola collapses
	}
	\label{fig:charact_curves_vis_bgmlv0}
\end{subfigure}
\caption{
	Characteristic curves ($\chern_i^{\alpha,\beta}(v)=0$) of stability conditions
	associated to Chern characters $v$ with $\Delta(v) \geq 0$ and positive rank.
}
\label{fig:charact_curves_vis}
\end{figure}

\begin{definition}[$\beta_{\pm}$]
	\label{dfn:beta_pm}
	Given a formal Chern character $v$ with positive rank, we define $\beta_{\pm}(v)$ to be
	the $\beta$-coordinate of where $\Theta_v^{\pm}$ meets the $\beta$-axis:
	\[
		\beta_\pm(R,C\ell,D\ell^2) = \frac{C \pm \sqrt{C^2-2RD}}{R}
	\]
	\noindent
	In particular, this means $\beta_\pm(v)$ are the two roots of the quadratic
	equation $\chern_2^{\beta}(v)=0$.

	This definition will be extended to the rank 0 case in Definition \ref{dfn:beta_-_rank0}.
\end{definition}


\subsubsection{Rank Zero Case}
\label{subsubsect:rank-zero-case-charact-curves}

\begin{sagesilent}
from rank_zero_case_curves import Theta_v_plot
\end{sagesilent}

\begin{fact}[Geometry of Characteristic Curves in Rank 0 Case]
The following facts can be deduced from the formulae for $\chern_i^{\alpha, \beta}(v)$
as well as the restrictions on $v$, when $\chern_0(v)=0$ and $\chern_1(v)>0$:


\begin{minipage}{0.5\textwidth}
\begin{itemize}
	\item $V_v = \emptyset$
	\item $\Theta_v$ is a vertical line at $\beta=\frac{D}{C}$
		where $v=\left(0,C\ell,D\ell^2\right)$
\end{itemize}
\end{minipage}
\hfill
\begin{minipage}{0.49\textwidth}
	\sageplot[width=\textwidth]{Theta_v_plot}
	%\caption{$\Delta(v)>0$}
	%\label{fig:charact_curves_rank0}
\end{minipage}
\end{fact}

We can view the characteristic curves for $\left(0,C\ell, D\ell^2\right)$ with $C>0$ as
the limiting behaviour of those of $\left(\varepsilon, C\ell, D\ell^2\right)$.
Indeed:
\begin{align*}
	\mu\left(\varepsilon, C\ell, D\ell^2\right) = \frac{C}{\varepsilon} &\longrightarrow +\infty
	\\
	\text{as} \:\: 0<\varepsilon &\longrightarrow 0
\end{align*}
So we can view $V_v$ as moving off infinitely to the right, with $\Theta_v^+$ even further.
But also, considering the base point of $\Theta_v^-$:
\begin{align*}
	\beta_{-}\left(\varepsilon, C\ell, D\ell^2\right) = \frac{C - \sqrt{C^2-2D\varepsilon}}{\varepsilon}
	&\longrightarrow \frac{D}{C}
	\\
	\text{as} \:\: 0<\varepsilon &\longrightarrow 0
	&\text{(via L'H\^opital)}
\end{align*}

So we can view $\Theta_v^-$ as approaching the vertical line that $\Theta_v$  becomes.
For this reason, I will refer to the whole of $\Theta_v$ in the rank zero case
as $\Theta_v^-$ to be able to use the same terminology in both positive rank
and rank zero cases.

\begin{definition}[Extending $\beta_-$ to rank 0 case]
	\label{dfn:beta_-_rank0}
	Given a formal Chern character $v$ with rank 0 and $\chern_1(v)>0$, we define
	$\beta_-(v)$ to be the $\beta$-coordinate of point where $\Theta_v$ meets the
	$\beta$-axis:
	\[
		\beta_-(0,C\ell,D\ell^2) = \frac{D}{C}
	\]
	\noindent
	If $\beta_+$ were also to be generalised to the rank 0 case, we would consider
	its value to be $+\infty$ due to the discussion above.
\end{definition}


\subsection{Relevance of \texorpdfstring{$V_v$}{V_v}}
\label{subsect:relevance-of-V_v}

For the positive rank case, by definition of the first tilt $\firsttilt\beta$, objects of Chern character
$v$ can only be in $\firsttilt\beta$ on the left of $V_v$, where
$\chern_1^{\alpha,\beta}(v)>0$, and objects of Chern character $-v$ can only be
in $\firsttilt\beta$ on the right, where $\chern_1^{\alpha,\beta}(-v)>0$. In
fact, if there is a Gieseker semistable coherent sheaf $E$ of Chern character
$v$, then $E \in \firsttilt\beta$ if and only if $\beta<\mu(E)$ (left of the
$V_v$), and $E[1] \in \firsttilt\beta$ if and only if $\beta\geq\mu(E)$.
Because of this, when using these characteristic curves, only positive ranks are
considered, as negative rank objects are implicitly considered on the right hand
side of $V_v$.

In the rank zero case, this still applies if we consider $V_v$ to be
`infinitely to the right' ($\mu(v) = +\infty$). Precisely, Gieseker semistable
coherent sheaves $E$ of Chern character $v$ are contained in
$\firsttilt{\beta}$ for all $\beta$



\subsection{Relevance of \texorpdfstring{$\Theta_v$}{Θ_v}}

Since $\chern_2^{\alpha, \beta}$ is the numerator of the tilt slope
$\nu_{\alpha, \beta}$. The curve $\Theta_v$, where this is 0, firstly divides the
$(\alpha$-$\beta)$-half-plane into regions where the signs of tilt slopes of
objects of Chern character $v$ (or $-v$) are fixed. Secondly, it gives more of a
fixed target for some $u=(r,c\ell,d\frac{1}{2}\ell^2)$ to be a
pseudo-semistabiliser of $v$, in the following sense: If $(\alpha,\beta)$, is on
$\Theta_v$, then for any $u$, $u$ can only be a pseudo-semistabiliser of $v$ if
$\nu_{\alpha,\beta}(u)=0$, and hence $\chern_2^{\alpha, \beta}(u)=0$. In fact,
this allows us to use the characteristic curves of some $v$ and $u$ (with
$\Delta(v), \Delta(u)\geq 0$ and positive ranks) to determine the location of
the pseudo-wall where $u$ pseudo-semistabilizes $v$. This is done by finding the
intersection of $\Theta_v$ and $\Theta_u$, the point $(\beta,\alpha)$ where
$\nu_{\alpha,\beta}(u)=\nu_{\alpha,\beta}(v)=0$, and a pseudo-wall point on
$\Theta_v$, and hence the apex of the circular pseudo-wall with centre $(\beta,0)$
(as per Subsection \ref{subsect:bertrams-nested-walls}).


\subsection{Bertram's Nested Wall Theorem}
\label{subsect:bertrams-nested-walls}

Although Bertram's nested wall Theorem can be proved more directly, it's also
important for the content of this document to understand the connection with
these characteristic curves.
Emanuele Macri noticed in (TODO ref) that any circular wall of $v$ reaches a critical
point on $\Theta_v$ (TODO ref). This is a consequence of
$\frac{\delta}{\delta\beta} \chern_2^{\alpha,\beta} = -\chern_1^{\alpha,\beta}$.
This fact, along with the hindsight knowledge that non-vertical walls are
circles with centers on the $\beta$-axis, gives an alternative view to see that
the circular walls must be nested and non-intersecting.
