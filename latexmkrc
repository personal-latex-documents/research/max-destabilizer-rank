@default_files = ('tex/main.tex');
$pdf_mode = 1;
$pdflatex = 'lualatex -shell-escape -synctex=1 -interaction=nonstopmode';
@generated_exts = (@generated_exts, 'synctex.gz');
$use_make_for_missing_files = 1;
$do_cd = 1;
$aux_dir = 'aux_files';
$out_dir = '../latex_output';
$jobname = 'max-destabilizer-rank';
