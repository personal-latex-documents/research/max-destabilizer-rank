% NOTE: SURFACE SPECIALIZATION
% (come back to these when adjusting to general Picard rank 1)
In this document we concentrate on two surfaces: Principally polarized abelian
surfaces and the projective surface $\PP^2$. Although this can be generalised
for Picard rank 1 surfaces, the formulae will need adjusting.
The Bridgeland stability conditions (defined in \cite{Bridgeland_StabK3}) are
given by two parameters $\alpha \in \RR_{>0}$, $\beta \in \RR$, which will be
illustrated throughout this article with diagrams of the upper half plane.

It is well known that for any rational $\beta_0$,
the vertical line $\{\sigma_{\alpha,\beta_0} \colon \alpha \in \RR_{>0}\}$ only
intersects finitely many walls
\cite[Thm 1.1]{LoJason2014Mfbs}
\cite[Prop 4.2]{alma9924569879402466}
\cite[Lemma 5.20]{MinaHiroYana_SomeModSp}.
A consequence of this is that if
$\beta_{-}$ is rational, then there can only be finitely many circular walls to the
left of the vertical wall $\beta = \mu$.
On the other hand, when $\beta_{-}$ is not rational,
\cite{yanagidaBridgelandStabilitiesAbelian2014}
showed that there are infinitely many walls.

This dichotomy does not only hold for real walls, realised by actual objects in
$\bddderived(X)$, but also for pseudowalls. Here pseudowalls are defined as
`potential' walls, induced by hypothetical Chern characters of semistabilisers
which satisfy certain numerical conditions which would be satisfied by any real
destabiliser, regardless of whether they are realised by actual semistabilisers
in $\bddderived(X)$ (Definition \ref{dfn:pseudo-semistabilizer}).

Since real walls are a subset of pseudowalls, the irrational $\beta_{-}$ case
follows immediately from the corresponding case for real walls.
However, the rational $\beta_{-}$ case involves showing that the following
conditions only admit finitely many solutions (despite the fact that the same
conditions admit infinitely many solutions when $\beta_{-}$ is irrational).


For a semistabilizing sequence
$E \hookrightarrow F \twoheadrightarrow G$ in $\mathcal{B}^\beta$
we have the following conditions.
There are some Bogomolov-Gieseker inequalities:
$0 \leq \Delta(E), \Delta(G)$.
We also have a condition relating to the tilt category $\firsttilt\beta$:
$0 \leq \chern^\beta_1(E) \leq \chern^\beta_1(F)$.
Finally, there is a condition ensuring that the radius of the circular wall is
strictly positive: $\chern^{\beta_{-}}_2(E) > 0$.

For any fixed $\chern_0(E)$, the inequality
$0 \leq \chern^{\beta}_1(E) \leq \chern^{\beta}_1(F)$,
allows us to bound $\chern_1(E)$. Then, the other inequalities allow us to
bound $\chern_2(E)$. The final part to showing the finiteness of pseudowalls
would be bounding $\chern_0(E)$. This has been hinted at in
\cite{SchmidtBenjamin2020Bsot} and done explicitly by Benjamin Schmidt within a
SageMath \cite{sagemath} library which computes pseudowalls
\cite{SchmidtGithub2020}.
Here we discuss these bounds in more detail, along with the methods used,
followed by refinements on them which give explicit formulae for tighter bounds
on $\chern_0(E)$ of potential destabilisers $E$ of $F$.
