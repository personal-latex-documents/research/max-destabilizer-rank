# Find the maximal rank for a destabilizing subobject or quotient and
# the minimal radius squared of our walls.
if s == beta_minus(v):
    p = 0
    n = Integer(s.denominator())
    m = Integer(1/var.gens[2])
    r_max = (n * ch(v, s)[1] - 1) ** 2 * m / gcd(2 * n ** 2, m)
