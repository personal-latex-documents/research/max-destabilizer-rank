\section{Definitions: Clarifying `pseudo'}

Throughout this Part, as noted in the introduction, we will be exclusively
working over surfaces $X$ with Picard rank 1, with a choice of ample line bundle
$L$ such that $\ell\coloneqq c_1(L)$ generates $NS(X)$.
We take $m\coloneqq \ell^2$ as this will be the main quantity which will
affect the results.

\begin{definition}[Pseudo-semistabilisers]
\label{dfn:pseudo-semistabilizer}
% NOTE: SURFACE SPECIALIZATION
	Given a Chern Character $v$, and a given stability
	condition $\sigma_{\alpha,\beta}$,
	a \emph{pseudo-semistabilising} $u$ is a `potential' Chern character:
	\[
		u = \left(r, c\ell, \frac{e}{\lcm(m,2)} \ell^2\right)
		\qquad
		r,c,e \in \ZZ
	\]
	which has the same tilt slope as $v$:
	$\nu_{\alpha,\beta}(u) = \nu_{\alpha,\beta}(v)$.

	\noindent
	Furthermore the following inequalities are satisfied:
	\begin{itemize}
		\item $\Delta(u) \geq 0$
		\item $\Delta(v-u) \geq 0$
		\item $0 \leq \chern_1^{\beta}(u) \leq \chern_1^{\beta}(v)$
	\end{itemize}

\end{definition}
\begin{remark}
	Note $u$ does not need to be a Chern character of an actual sub-object of some
	object in the stability condition's heart with Chern character $v$.

	Other sources may also have extra conditions such as the integrality of Euler
	characteristics or the non-triviality of certain extension groups.
	These conditions depend on the Todd class of the variety in question and could
	be considered but are left out for now as they do not have a great impact on
	the finiteness of pseudo-walls.
	In the case of a principally polarised abelian surface, the main example in
	this thesis, the Euler characteristic condition is vacuous.
    The extension group condition can be shown to only
    be redundant among the other conditions for sufficiently large rank of $u$
    (when $\chern_0(u) \geq \chern_0(v)/2$), so does not affect the finiteness
    of pseudo-semistabilisers.
\end{remark}

Later, Chern characters will be written $(r,c\ell,d\ell^2)$ because operations
(such as multiplication) are more easily defined in terms of the coefficients of
the $\ell^i$. However, at the end, it will become important again that
$d \in \frac{1}{\lcm(m,2)}\ZZ$.

\begin{definition}[Pseudo-walls]
\label{dfn:pseudo-wall}
	Let $u$ be a pseudo-semistabiliser of $v$, for some stability condition.
	Then the \emph{pseudo-wall} associated to $u$ is the set of all stablity
	conditions where $u$ is a pseudo-semistabiliser of $v$.
\end{definition}

% TODO possibly reference forwards to Bertram's nested wall Theorem section to
% cover that being a pseudo-semistabiliser somewhere implies also on whole circle

\begin{lemma}[Sanity check for Pseudo-semistabilisers]
\label{lem:sanity-check-for-pseudo-semistabilizers}
	Given a stability
	condition $\sigma_{\alpha,\beta}$,
	if $E\hookrightarrow F\twoheadrightarrow G$ is a semistabilising sequence in
	$\firsttilt\beta$ for $F$.
	Then $\chern(E)$ is a pseudo-semistabiliser of $\chern(F)$
\end{lemma}

\begin{proof}
	Suppose $E\hookrightarrow F\twoheadrightarrow G$ is a semistabilising
	sequence with respect to a stability condition $\sigma_{\alpha,\beta}$.
	\begin{equation*}
		\chern(E) = -\chern(\homol^{-1}_{\coh}(E)) + \chern(\homol^{0}_{\coh}(E))
	\end{equation*}
	Therefore, $\chern(E)$ is of the form
	$(r,c\ell,\frac{e}{\lcm(m,2)}\ell^2)$
	provided that this is true for any coherent sheaf.
	For any coherent sheaf $H$, we have the following:
	\begin{equation*}
		\chern(H) = \left(c_0(H), c_1(H), - c_2(H) + \frac{1}{2} {c_1(H)}^2\right)
	\end{equation*}
	Given that $\ell$ generates the Neron-Severi group, $c_1(H)$ can then be
	written $c\ell$.
	\begin{equation*}
		\chern(H) = \left(
			c_0(H), c\ell,
			\left(- \frac{c_2(H)}{\ell^2} + \frac{c^2}{2} \right)\ell^2
		\right)
	\end{equation*}
	This fact along with $c_0$, $c_2$ being an integers on surfaces, and
	$m\coloneqq \ell^2$ implies that $\chern(H)$
	(hence $\chern(E)$ too) is of the required form.


	Since all the objects in the sequence are in $\firsttilt\beta$, we have
	$\chern_1^{\beta} \geq 0$ for each of them. Due to additivity
	($\chern(F) = \chern(E) + \chern(G)$), we can deduce
	$0 \leq \chern_1^{\beta}(E) \leq \chern_1^{\beta}(F)$.


	$E \hookrightarrow F \twoheadrightarrow G$ being a semistabilising sequence
	means	$\nu_{\alpha,\beta}(E) = \nu_{\alpha,\beta}(F) = \nu_{\alpha,\beta}(F)$.
	% MAYBE: justify this harder
	But also, that this is an instance of $F$ being semistable, so $E$ must also
	be semistable
	(otherwise the destabilising subobject would also destabilise $F$).
	Similarly $G$ must also be semistable too.
	$E$ and $G$ being semistable implies they also satisfy the Bogomolov
	inequalities (Theorem \ref{lem:bogomolov-inequality}):
	% TODO ref Bogomolov inequalities for tilt stability
	$\Delta(E), \Delta(G) \geq 0$.
	Expressing this in terms of Chern characters for $E$ and $F$ gives:
	$\Delta(\chern(E)) \geq 0$ and $\Delta(\chern(F)-\chern(E)) \geq 0$.

\end{proof}

\section{Characteristic Curves for Pseudo-semistabilisers}

These characteristic curves introduced in
Subsection \ref{subsec:charact-curves}
are convenient tools to think about the
numerical conditions that can be used to test for pseudo-semistabilisers, and
for solutions to the problems
(\ref{problem:problem-statement-1},\ref{problem:problem-statement-2})
tackled in this Part (to be introduced later).
In particular, these problems will be translated to
a list of numerical inequalities on its solutions $u$.
% ref to appropriate Lemma when it's written

The next Lemma is a key to making this translation and revolves around the
geometry and configuration of the characteristic curves involved in a
semistabilising sequence.

\begin{lemma}[Numerical tests for left-wall pseudo-semistabilisers]
\label{lem:pseudo_wall_numerical_tests}
Let $v$ and $u$ be Chern characters with $\Delta(v),
\Delta(u)\geq 0$, and $v$ is positive, with one of $\chern_0(v)$ or
$\chern_1(v)$ non-zero.
Let $P=(\alpha_0, \beta_0)$ be a point on $\Theta_v^-$.

\noindent
The following conditions:
\begin{enumerate}[label=\alph*]
\item $u$ is a pseudo-semistabiliser of $v$ at some point on $\Theta_v^-$ above
	$P$
\item $u$ destabilises $v$ going `inwards', that is,
	$\nu_{\alpha,\beta}(u)<\nu_{\alpha,\beta}(v)$ outside the pseudo-wall, and
	$\nu_{\alpha,\beta}(u)>\nu_{\alpha,\beta}(v)$ inside.
\end{enumerate}

\noindent
are equivalent to the following more numerical conditions:
\begin{enumerate}
	\item $u$ has positive rank
	\item $\beta_0<\mu(u)<\mu(v)$, i.e. $V_u$ is strictly between $P$ and $V_v$.
		\label{lem:ps-wall-num-test:num-cond-slope}
	\item $\chern_1^{\beta_0}(u) < \chern_1^{\beta_0}(v)$
	\item $\Delta(v-u) \geq 0$
	\item $\chern_2^{\alpha_0,\beta_0}(u)>0$
\end{enumerate}
\end{lemma}

\begin{proof}
First, consider the case where $\chern_0(v)>0$.
Let $u,v$ be Chern characters such that
$\Delta(u),\Delta(v) \geq 0$, and $v$ has positive rank.

For the forwards implication, assume that the suppositions of the Lemma are
satisfied. Let $Q$ be the point on $\Theta_v^-$ (above $P$) where $u$ is a
pseudo-semistabiliser of $v$.
Firstly, consequence 4 is part of the definition for $u$ being a
pseudo-semistabiliser at a point with same $\beta$ value of $P$ (since the
pseudo-wall surrounds $P$).
Also, $u$ is a pseudo-semistabiliser along the pseudo-wall
which surround $P$, so by definition of pseudo-semistabilisers,
${
    0 \leq \chern_1^\beta(u) \leq \chern_1^\beta(v)
}$
for all $(\alpha, \beta)$ on the pseudo-wall.
In particular, this holds for $\beta$ in a neighbourhood of $\beta_0$,
so we can conclude
${
    0 < \chern_1^{\beta_0}(u) < \chern_1^{\beta_0}(v)
}$
(consequence 5).
Notice that $0 < \chern_1^{\beta_0}(u)$
follows from consequences 1 and 2, this is why it is not included in consequence 5.
If $u$ were to have 0 rank, its tilt slope would be decreasing as $\beta$
increases, contradicting supposition b. So $u$ must have strictly non-zero rank,
and we can consider its characteristic curves (or that of $-u$ in case of
negative rank).
$\nu_Q(v)=0$, and hence $\nu_Q(u)=0$ too. This means that $\Theta_{\pm u}$ must
intersect $\Theta_v^-$ at $Q$. Considering the shapes of the hyperbolae alone,
there are 3 distinct ways that they can intersect, as illustrated in Fig
\ref{fig:hyperbol-intersection}. These cases are distinguished by whether it is
the left, or the right branch of $\Theta_u$ involved, as well as the positions
of the base. However, considering supposition b, only case 3 (green in
figure) is possible. This is because we need $\nu_{P}(u)>0$ (or $\nu_{P}(-u)>0$ in
case 1 involving $\Theta_u^+$), to satisfy supposition b.
Recalling how the sign of $\nu_{\alpha,\beta}(\pm u)$ changes (illustrated in
Fig \ref{fig:charact_curves_vis}), we can eliminate cases 1 and 2.

\begin{sagesilent}
from characteristic_curves import \
hyperbola_intersection_plot, \
correct_hyperbola_intersection_plot
\end{sagesilent}

\begin{figure}
\begin{subfigure}[t]{0.48\textwidth}
	\centering
	\sageplot[width=\textwidth]{hyperbola_intersection_plot()}
	\caption{Three ways the characteristic hyperbola for $u$ can intersect the left
	branch of the characteristic hyperbola for $v$}
	\label{fig:hyperbol-intersection}
\end{subfigure}
\hfill
\begin{subfigure}[t]{0.48\textwidth}
	\centering
	\sageplot[width=\textwidth]{correct_hyperbola_intersection_plot()}
	\caption{Closer look at characteristic curves for valid case}
	\label{fig:correct-hyperbol-intersection}
\end{subfigure}
\end{figure}

Fixing attention on the only possible case (2), illustrated in Fig
\ref{fig:correct-hyperbol-intersection}.
$P$ is on the left of $V_{\pm u}$ (first part of consequence 2), so $u$ must
have positive rank (consequence 1)
to ensure that $\chern_1^{\beta_0} \geq 0$ (since the pseudo-wall passed over
$P$).
Furthermore, $P$ being on the left of $V_u$ implies
$\chern_1^{\beta_0}(u) > 0$,
and therefore $\chern_2^{P}(u) > 0$ (consequence 5) to satisfy supposition b.
Next considering the way the hyperbolae intersect, we must have $\Theta_u^-$ taking a
base-point to the right $\Theta_v$, but then, further up, crossing over to the
left side. The latter fact implies that the assymptote for $\Theta_u^-$ must be
to the left of the one for $\Theta_v^-$. Given that they are parallel and
intersect the $\beta$-axis at $\beta=\mu(u)$ and $\beta=\mu(v)$ respectively. We
must have $\mu(u)<\mu(v)$ (second part of consequence 2),
that is, $V_u$ is strictly to the left of $V_v$.


Conversely, suppose that the consequences 1-5 are satisfied. Consequence 2
implies that the assymptote for $\Theta_u^-$ is to the left of $\Theta_v^-$.
Consequences 4 and 5, along with $\beta_0<\mu(u)$, implies that $P$ must be in the
region left of $\Theta_u^-$. These two facts imply that $\Theta_u^-$ is to the
right of $\Theta_v^-$ at $\alpha=\alpha(P)$, but crosses to the left side as
$\alpha \to +\infty$, intersection at some point $Q$ above $P$.
This implies that the characteristic curves for $u$ and $v$ are in the
configuration illustrated in Fig \ref{fig:correct-hyperbol-intersection}.
We then have $\nu(u)=\nu(v)$ along a circle to the left of $V_u$ reaching its
apex at $Q$, and encircling $P$. This along with consequence 3 implies that $u$
is a pseudo-semistabiliser at the point on the circle with $\beta=\beta_0$.
Therefore, it is also a pseudo-semistabiliser further along the circle at $Q$
(supposition a).
Finally, consequence 4 along with $P$ being to the left of $V_u$ implies
$\nu_P(u) > 0$ giving supposition b.

\begin{sagesilent}
from rank_zero_case_curves import pseudo_semistab_char_curves_rank_zero
\end{sagesilent}
\begin{figure}
	\centering
	\sageplot[width=\textwidth]{pseudo_semistab_char_curves_rank_zero}
	\caption{characteristic curves configuration for pseudo-semistabiliser of $v$
	with rank 0 destabilising `downwards'}
	\label{fig:hyperbol-intersection-rank-zero}
\end{figure}

Now consider the case where $\chern_0(v)=0$ but $\chern_1(v)>0$.
Let $u,v$ be Chern characters with
$\Delta(u),\Delta(v) \geq 0$, and $\chern_0(v)=0$ but $\chern_1(v)>0$.
So $\Theta_v^-$ is the vertical line at $\beta = \beta_{-}(v)$, and
$\mu(v) = +\infty$.

For the forward implication, assume suppositions a and b hold.
Let $Q$ be the point above $P$ on $\Theta_v^-$ where $u$ is a
pseudo-semistabiliser of $v$. So $\Theta_u$ intersects $\Theta_v^-$ at $Q$.
Suppose, seeking a contradiction that $\chern_0(u)=0$,
then $\Theta_u = \Theta_u^-$ is also a vertical line,
and must then be the same line as $\Theta_v^-$ to intersect $Q$.
This would imply $\QQ u = \QQ v$ and then $u$ would not destabilise $v$ at any
stability condition.
So we must have either $\chern_0(u) < 0$, in which case $\Theta_u^+$ is the
branch of $\Theta_u$ going through $Q$; or $\chern_0(u) > 0$, in which it is
$\Theta_u^-$ instead.
The latter case is the only one which could satisfy supposition b, about $u$
destabilising $v$ going `down' $\Theta_v^-$.
Which then forces the configuration of characteristic curves shown in Figure
\ref{fig:hyperbol-intersection-rank-zero}.
The positions of the characteristic curves ensures the numerical conditions 1, 2
and 5. The other conditions follow from the definition of $u$ being a
pseudo-semistabiliser of $v$.

Conversely, suppose that the numerical conditions 1-5 are satisfied,
then this forces the configuration of characteristic curves shown in Figure
\ref{fig:hyperbol-intersection-rank-zero}.
This ensures that $u$ is a pseudo-semistabiliser of $v$ at $u$ destabilising
going down $\Theta_v^-$.
\end{proof}

\begin{remark}
	Given a fixed positive $v$ with $\Delta(v)\geq 0$,
	for any $u$ satisfying the numerical conditions of this Lemma,
	with some given fixed rank ($\chern_0(u)$),
	condition \ref{lem:ps-wall-num-test:num-cond-slope}
	gives us bounds for $\chern_1(u)$. For any fixed values for
	$\chern_0(u)$ and $\chern_1(u)$, the last three conditions will give bounds
	for $\chern_2(u)$.
	These bounds exist regardless of any extra assumptions about $v$.
	However any bounds on $\chern_0(u)$ are less immediate from these numerical
	conditions.
	The semistabiliser $E$ for the largest `left' wall for $v$ must destabilise a Gieseker
	stable sheaf $F$ with $\chern(F)=v$. We would have a short exact sequence in
	the heart of a stability condition on the wall given by
	\[
		0 \to E \hookrightarrow F \twoheadrightarrow G \to 0
	\]
	for some $G$ in the heart.
	Considering cohomology over $\coh(X)$, and using the fact that $F$ is a sheaf,
	we get an exact sequence in $\coh(X)$ given by:
	\[
		0 \to
		\cancelto{0}{\cohom^{-1}(E)} \to
		0 \to
		\cohom^{-1}(G) \to
		\cohom^{0}(E) \to
		\cohom^{0}(F) \to
		\cohom^{0}(G) \to
		0
	\]
	So we do have that $E$ must be a sheaf, but not necessarily a subsheaf of $F$,
	and so $\chern_0(E)$ is not necessarily smaller than $\chern_0(F)$.
	Furthermore, for smaller walls, the semistabiliser may not even be a sheaf.

	When choosing the base-point of $\Theta_v^{-}$: $P=(\beta_{-}(v), 0)$, we
	will see in Part \ref{part:inf-walls} that there can be infinitely many walls
	when $\beta_{-}(v)$ is irrational,
	hence infinitely many $u$ satisfying the above.
	Therefore any construction of a bound on the ranks of possible $u$
	must rely on the rationality of $\beta_0$ in this case.
\end{remark}


\section{The Problem: Finding Pseudo-walls}

As hinted in the introduction to this Part \ref{part:fin-walls}, the main motivation of the
results in this article are not only the bounds on pseudo-semistabiliser
ranks;
but also applications for finding a list (comprehensive or subset) of
pseudo-walls.

After introducing the characteristic curves of stability conditions associated
to a fixed Chern character $v$, we can now formally state the problems that we
are trying to solve for.

\subsection{Problem statements}

\begin{problem}[sufficiently large `left' pseudo-walls]
\label{problem:problem-statement-1}

Fix a Chern character $v$ with non-negative rank (and $\chern_1(v)>0$ if rank 0),
and $\Delta(v) \geq 0$.
The goal is to find all pseudo-semistabilisers $u$
which give circular pseudo-walls containing some fixed point
$P\in\Theta_v^-$.
With the added restriction that $u$ `destabilises' $v$ moving `inwards', that is,
$\nu(u)>\nu(v)$ inside the circular pseudo-wall.
\end{problem}
This will give all pseudo-walls between the chamber corresponding to Gieseker
stability and the stability condition corresponding to $P$.
The purpose of the final `direction' condition is because, up to that condition,
semistabilisers are not distinguished from their corresponding quotients:
Suppose $E\hookrightarrow F\twoheadrightarrow G$, then the tilt slopes
$\nu_{\alpha,\beta}$
are strictly increasing, strictly decreasing, or equal across the short exact
sequence (consequence of the see-saw principle).
In this case, $\chern(E)$ is a pseudo-semistabiliser of $\chern(F)$, if and
only if $\chern(G)$ is a pseudo-semistabiliser of $\chern(F)$.
The numerical inequalities in the definition for pseudo-semistabiliser cannot
tell which of $E$ or $G$ is the subobject.
However, what can be distinguished is the direction across the wall that
$\chern(E)$ or $\chern(G)$ destabilises $\chern(F)$
(they will each destabilise in the opposite direction to the other).
The `inwards' semistabilisers are preferred because we are moving from a
typically more familiar chamber
(the stable objects of Chern character $v$ in the outside chamber will only be
Gieseker stable sheaves).

\begin{remark}
	Also note that this last restriction does not remove any pseudo-walls found,
	and if we do want to recover `outwards' semistabilisers, we can simply take
	$v-u$ for each solution $u$ of the problem.
\end{remark}


\begin{problem}[all `left' pseudo-walls]
\label{problem:problem-statement-2}

Fix a Chern character $v$ with non-negative rank (and $\chern_1(v)>0$ if rank 0),
$\Delta(v) \geq 0$, and $\beta_{-}(v) \in \QQ$.
The goal is to find all pseudo-semistabilisers $u$ which give circular
pseudo-walls on the left side of $V_v$.
Where $u$ destabilises $v$ going `down' $\Theta_v^{-}$ (in the same sense as in
Problem \ref{problem:problem-statement-1}.
\end{problem}

This is a specialisation of Problem \ref{problem:problem-statement-1}
with the choice $P=(\beta_{-},0)$, the point where $\Theta_v^-$ meets the
$\beta$-axis.
This is because all circular walls left of $V_v$ intersect $\Theta_v^-$ (once).
The $\beta_{-}(v) \in \QQ$ condition is to ensure that there are finitely many
solutions. As mentioned in the introduction to this Part, this is known,
however this will also be proved again implicitly in Chapter
\ref{chapt:computing-semistabilisers},
where an algorithm is produced to find all solutions.

This description still holds for the case of rank 0 case if we consider $V_v$ to
be infinitely far to the right
(see Section \ref{subsubsect:rank-zero-case-charact-curves}).
Note also that the condition on $\beta_-(v)$ always holds for $v$ rank 0.

\subsection{Numerical Formulations of the Problems}

The problems introduced in this section are phrased in the context of stability
conditions. However, these can be reduced down completely to purely numerical
problem using Lemma \ref{lem:pseudo_wall_numerical_tests}.

\begin{theorem}[Numerical Tests for Sufficiently Large `left' Pseudo-walls]
	\label{lem:num_test_prob1}
	Given a Chern character $v$ with non-negative rank
	(and $\chern_1(v)>0$ if rank 0),
	and $\Delta(v) \geq 0$,
	and a choice of point $P=(\alpha_0, \beta_0)$ on $\Theta_v^-$.
	Solutions $u=(r,c\ell,d\ell^2)$
	to Problem \ref{problem:problem-statement-1}.
	Are precisely given by $r,c \in \ZZ$, $d \in \frac{1}{\lcm(m,2)}$
	satisfying the following conditions:
	\begin{enumerate}
		\begin{multicols}{2}
		\item $r > 0$
			\label{item:rankpos:lem:num_test_prob1}
		\item $\Delta(u) \geq 0$
			\label{item:bgmlvu:lem:num_test_prob1}
		\item $\Delta(v-u) \geq 0$
			\label{item:bgmlvv-u:lem:num_test_prob1}
		\item $\mu(u)<\mu(v)$
		\item $0<\chern_1^{\beta_0}(u)<\chern_1^{\beta_0}(v)$
			\label{item:chern1bound:lem:num_test_prob1}
		\item $\chern_2^{\alpha_0,\beta_0}(u)>0$
			\label{item:radiuscond:lem:num_test_prob1}
		\end{multicols}
	\end{enumerate}
\end{theorem}

\begin{proof}
	Consider the context of $v$ being a Chern character with non-negative rank
	(and with $\chern_1(v)>0$ if rank 0)
	and
	$\Delta \geq 0$, and $u$ being a Chern character with $\Delta(u) \geq 0$.
	Lemma \ref{lem:pseudo_wall_numerical_tests} gives that the remaining
	conditions for $u$ being a solution to Problem
	\ref{problem:problem-statement-1} are precisely equivalent to the
	remaining conditions in this Lemma.
	% TODO maybe make this more explicit
	% (the conditions are not exactly the same)

\end{proof}

\begin{theorem}[Numerical Tests for All `left' Pseudo-walls]
\label{cor:num_test_prob2}
	Given a Chern character $v$ with non-negative rank
	(and $\chern_1(v)>0$ if rank 0),
	and $\Delta(v) \geq 0$,
	such that $\beta_{-}\coloneqq\beta_{-}(v) \in \QQ$.
	Solutions $u=(r,c\ell,d\ell^2)$
	to Problem \ref{problem:problem-statement-2}.
	Are precisely given by $r,c \in \ZZ$, $d\in\frac{1}{\lcm(m,2)}\ZZ$ satisfying
	the following conditions:
	\begin{enumerate}
		\begin{multicols}{2}
		\item $r > 0$
			\label{item:rankpos:lem:num_test_prob2}
		\item $\Delta(u) \geq 0$
			\label{item:bgmlvu:lem:num_test_prob2}
		\item $\Delta(v-u) \geq 0$
			\label{item:bgmlvv-u:lem:num_test_prob2}
		\item $\mu(u)<\mu(v)$
			\label{item:mubound:lem:num_test_prob2}
		\item $0<\chern_1^{\beta_{-}}(u)<\chern_1^{\beta_{-}}(v)$
			\label{item:chern1bound:lem:num_test_prob2}
		\item $\chern_2^{\beta_{-}}(u)>0$
			\label{item:radiuscond:lem:num_test_prob2}
		\end{multicols}
	\end{enumerate}
\end{theorem}

\begin{proof}
	This is a specialisation of the previous Theorem
	\ref{lem:num_test_prob1},
    using $P=(\beta_{-},0)$.

\end{proof}
